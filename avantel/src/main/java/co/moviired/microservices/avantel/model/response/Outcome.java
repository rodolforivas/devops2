package co.moviired.microservices.avantel.model.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;
import org.springframework.http.HttpStatus;

/*
 * Copyright @2018. SBD, SAS. Todos los derechos reservados.
 *
 * @author RIVAS, Rodolfo
 * @version 1, 2018-11-02
 * @since 1.0
 */
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "message",
        "statusCode",
        "error"
})
@Data
public class Outcome {

    private String message;
    private Integer statusCode;
    private ErrorDetail error;

    // CONSTRUCTORES

    public Outcome() {
        super();
    }

    public Outcome(HttpStatus statusCode, ErrorDetail error) {
        super();
        this.statusCode = statusCode.value();
        this.message = statusCode.getReasonPhrase();
        this.error = error;
    }

    public Outcome(Integer statusCode, ErrorDetail error) {
        super();
        this.error = error;

        switch (statusCode) {
            case 400: {
                this.statusCode = HttpStatus.BAD_REQUEST.value();
                this.message = HttpStatus.BAD_REQUEST.getReasonPhrase();
            }
            break;

            case 401: {
                this.statusCode = HttpStatus.UNAUTHORIZED.value();
                this.message = HttpStatus.UNAUTHORIZED.getReasonPhrase();
            }
            break;

            case 403: {
                this.statusCode = HttpStatus.FORBIDDEN.value();
                this.message = HttpStatus.FORBIDDEN.getReasonPhrase();
            }
            break;

            case 408: {
                this.statusCode = HttpStatus.REQUEST_TIMEOUT.value();
                this.message = HttpStatus.REQUEST_TIMEOUT.getReasonPhrase();
            }
            break;

            case 503: {
                this.statusCode = HttpStatus.SERVICE_UNAVAILABLE.value();
                this.message = HttpStatus.SERVICE_UNAVAILABLE.getReasonPhrase();
            }
            break;

            default: {
                this.statusCode = HttpStatus.INTERNAL_SERVER_ERROR.value();
                this.message = HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase();
            }
        }
    }
}
