package co.moviired.microservices.avantel.exception;


import co.moviired.microservices.avantel.model.enums.ErrorType;

/*
 * Copyright @2018. SBD, SAS. Todos los derechos reservados.
 *
 * @author RIVAS, Rodolfo
 * @version 1, 2018-11-02
 * @since 1.0
 */
public class ProcessingException extends ServiceException {

    public ProcessingException() {
        super(ErrorType.PROCESSING, "500", "Ha ocurrido un erro al procesar la petición");
    }

    public ProcessingException(String codigo, String mensaje) {
        super(ErrorType.PROCESSING, "500", mensaje);
    }

    public ProcessingException(Exception e) {
        super(ErrorType.PROCESSING, "500", e.getMessage());
    }

}
