package co.moviired.microservices.avantel.model.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/*
 * Copyright @2018. SBD, SAS. Todos los derechos reservados.
 *
 * @author RIVAS, Rodolfo
 * @version 1, 2018-11-02
 * @since 1.0
 */
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonInclude(JsonInclude.Include.NON_NULL)
@lombok.Data
@JsonPropertyOrder({
        "data",
        "outcome"
})
public class Response {

    private Data data;
    private Outcome outcome;

    // CONSTRUCTORES

    public Response() {
        super();
    }

    public Response(Outcome outcome, Data data) {
        super();
        this.outcome = outcome;
        this.data = data;
    }
}
