package co.moviired.microservices.avantel.exception;


import co.moviired.microservices.avantel.model.enums.ErrorType;

/*
 * Copyright @2018. SBD, SAS. Todos los derechos reservados.
 *
 * @author RIVAS, Rodolfo
 * @version 1, 2018-11-02
 * @since 1.0
 */
public class DataException extends ServiceException {

    public DataException() {
        super(ErrorType.DATA, "500", "Los datos provistos en la petición no son correctos o no se proporcionaron");
    }

    public DataException(String codigo, String mensaje) {
        super(ErrorType.DATA, "500", mensaje);
    }

    public DataException(Exception e) {
        super(ErrorType.DATA, "500", e.getMessage());
    }

}
