package co.moviired.microservices.avantel.model.response;

import co.moviired.microservices.avantel.exception.ServiceException;
import co.moviired.microservices.avantel.model.enums.ErrorType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

/*
 * Copyright @2018. SBD, SAS. Todos los derechos reservados.
 *
 * @author RIVAS, Rodolfo
 * @version 1, 2018-11-02
 * @since 1.0
 */
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "errorMessage",
        "errorType",
        "errorCode"
})
@Data
public class ErrorDetail {

    private String errorMessage;
    private Integer errorType;
    private String errorCode;

    // CONSTRUCTORES

    public ErrorDetail(ErrorType errorType, String errorCode, String errorMessage) {
        super();
        this.errorType = errorType.ordinal();
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public ErrorDetail(Integer errorType, String errorCode, String errorMessage) {
        super();
        this.errorType = errorType;
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public ErrorDetail(ServiceException se) {
        this(se.getTipo(), se.getCodigo(), se.getMensaje());
    }

}
