package co.moviired.microservices.avantel.controller;

import co.moviired.microservices.avantel.exception.DataException;
import co.moviired.microservices.avantel.exception.ServiceException;
import co.moviired.microservices.avantel.model.enums.ErrorType;
import co.moviired.microservices.avantel.model.request.Input;
import co.moviired.microservices.avantel.model.request.Request;
import co.moviired.microservices.avantel.model.response.Data;
import co.moviired.microservices.avantel.model.response.ErrorDetail;
import co.moviired.microservices.avantel.model.response.Outcome;
import co.moviired.microservices.avantel.model.response.Response;
import co.moviired.microservices.avantel.service.IAvantelService;
import co.moviired.microservices.avantel.service.impl.AvantelService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;


/*
 * Copyright @2018. SBD, SAS. Todos los derechos reservados.
 *
 * @author RIVAS, Rodolfo
 * @version 1, 2018-11-02
 * @since 1.0
 */
@CrossOrigin
@RestController("avantelWS")
@Slf4j
public class AvantelWS {

    private final IAvantelService avantelService;

    @Autowired
    public AvantelWS(AvantelService avantelService) {
        super();
        this.avantelService = avantelService;
    }

    // Método: PING
    @GetMapping(value = "${spring.application.services.rest.ping}")
    public Response ping() {
        Outcome result;

        try {
            log.info("**** Ping ws_AVANTEL: Iniciado ****");

            // Establecer la respuesta exitosa
            result = new Outcome(HttpStatus.OK, null);
            result.setError(null);

        } catch (Exception e) {
            // Capturar cualquier no esperado
            result = new Outcome(HttpStatus.INTERNAL_SERVER_ERROR, new ErrorDetail(ErrorType.PROCESSING, "-1", e.getMessage()));
        }
        log.info("**** Ping ws_AVANTEL: Finalizado ****");

        // Devolver la respuesta
        return new Response(result, null);
    }


    // Método: Recargar saldos
    @PostMapping(value = "${spring.application.services.rest.topup}")
    public Response recargar(@RequestBody Request req) {
        Outcome result;
        Data data;

        try {
            log.info("**** Recarga ws_AVANTEL: Iniciada ****");
            // Verificar los parámetros de entrada
            if (!(req.getData() instanceof HashMap))
                throw new DataException();
            Input parameters = Input.parseInput(req.getData());

            // Invocar al servicio de negocio con los datos suministrados
            data = this.avantelService.procesarRecarga(parameters);

            // Establecer la respuesta exitosa
            result = new Outcome(HttpStatus.OK, null);
            result.setError(null);

        } catch (ServiceException se) {
            // Establecer el error de procesamiento
            result = new Outcome(HttpStatus.PROCESSING, new ErrorDetail(se));
            data = null;

        } catch (Exception e) {
            // Capturar cualquier no esperado
            result = new Outcome(HttpStatus.INTERNAL_SERVER_ERROR, new ErrorDetail(ErrorType.PROCESSING, "-1", e.getMessage()));
            data = null;
        }
        log.info("**** Recarga ws_AVANTEL: Finalizada ****");

        // Devolver la respuesta
        return new Response(result, data);
    }

    // Método: Revertir Recargar saldos
    @PostMapping(value = "${spring.application.services.rest.reverseTopup}")
    public Response revertirRecarga(@RequestBody Request req) {
        Outcome result;
        Data data;

        try {
            log.info("**** Reversion de Recarga ws_AVANTEL: Iniciada ****");
            // Verificar los parámetros de entrada
            if (!(req.getData() instanceof HashMap))
                throw new DataException();
            Input parameters = Input.parseInput(req.getData());

            // Invocar al servicio de negocio con los datos suministrados
            data = this.avantelService.revertirRecarga(parameters);

            // Establecer la respuesta exitosa
            result = new Outcome(HttpStatus.OK, null);
            result.setError(null);

        } catch (ServiceException se) {
            // Establecer el error de procesamiento
            result = new Outcome(HttpStatus.PROCESSING, new ErrorDetail(se));
            data = null;

        } catch (Exception e) {
            // Capturar cualquier no esperado
            result = new Outcome(HttpStatus.INTERNAL_SERVER_ERROR, new ErrorDetail(ErrorType.PROCESSING, "-1", e.getMessage()));
            data = null;
        }
        log.info("**** Reversion Recarga ws_AVANTEL: Finalizada ****");

        // Devolver la respuesta
        return new Response(result, data);
    }
}
