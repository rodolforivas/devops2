package co.moviired.microservices.avantel.service.impl;

import co.moviired.microservices.avantel.exception.DataException;
import co.moviired.microservices.avantel.exception.ServiceException;
import co.moviired.microservices.avantel.model.WSConfig;
import co.moviired.microservices.avantel.model.enums.ErrorType;
import co.moviired.microservices.avantel.model.request.Input;
import co.moviired.microservices.avantel.model.response.Data;
import co.moviired.microservices.avantel.repository.IAvantelRepository;
import co.moviired.microservices.avantel.service.IAvantelService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/*
 * Copyright @2018. SBD, SAS. Todos los derechos reservados.
 *
 * @author RIVAS, Rodolfo
 * @version 1, 2018-11-02
 * @since 1.0
 */
@Service("avantelService")
@Slf4j
public class AvantelService implements IAvantelService {


    // Constantes
    private static final String SEPARATOR = "?";
    private static final Integer MAX_DIGITOS_RECHARGE_NUMBER = 12;
    private static final Integer MAX_DIGITOS_AMOUNT = 6;


    private static final int OIL_LENGHT = 11;
    private static final int LOTE_LENGHT = 10;
    private static final int AMOUNT_LENGHT = 11;


    // Componentes
    private final WSConfig config;
    private final IAvantelRepository avantelRepository;

    @Autowired
    public AvantelService(IAvantelRepository avantelRepository, WSConfig config) {
        super();
        log.info("Configuración del servicio: " + config.getServiceName() + " - INICIADA");

        this.avantelRepository = avantelRepository;
        this.config = config;
        log.info("Configuración del servicio: " + config.getServiceName() + " - EXITOSA");
    }

    // Crear la cadena de petición de RECARGAS/CONSULTA al operador
    private String createAvantelRequest(@NotNull Input params) throws DataException {
        String requestAvantel;

        // Validar Parámetros de Entrada
        this.validateInputRequest(params);

        // A. VALIDAR PARÁMETROS DE ENTRADA
        if (params.getControlNumber() == null)
            throw new DataException("-2", "El OID es un parámetro obligatorio");

        if (params.getRechargeNumber() == null)
            throw new DataException("-2", "El NÚMERO DE RECARGA es un parámetro obligatorio");

        if (params.getAmount() == null)
            throw new DataException("-2", "El VALOR es un parámetro obligatorio");


        String monto = params.getAmount() + "00";
        String oid = params.getControlNumber();
        String lote = this.config.getLot() == null ? "" : this.config.getLot();

        if (monto.length() < AMOUNT_LENGHT)
            monto = zerofill(monto, AMOUNT_LENGHT);

        if (lote.length() < LOTE_LENGHT)
            lote = zerofill(lote, LOTE_LENGHT);

        if (oid.length() < OIL_LENGHT)
            oid = zerofill(oid, OIL_LENGHT);

        String fechaVencimiento = getVigencia();

        // B. ARMAR EL MENSAJE
        //   01. CREDIPPE
        //   02. Entidad
        //   03. Telefono
        //   04. Monto
        //   05. Lote

        StringBuilder sb = new StringBuilder();
        sb.append("CREDIPPE:");
        sb.append("ENT=").append(this.config.getEntity()).append(SEPARATOR);
        sb.append("TEL=").append(params.getRechargeNumber()).append(SEPARATOR);
        sb.append("SDO=").append(monto).append(SEPARATOR);
        sb.append("LOT=").append(lote).append(SEPARATOR);
        sb.append("FECHAF=").append(fechaVencimiento).append(SEPARATOR);
        sb.append("OID=").append(oid).append(SEPARATOR);
        sb.trimToSize();
        requestAvantel = sb.toString();

        return requestAvantel;
    }

    // Crear la cadena de petición de RECARGAS/CONSULTA al operador
    private String createAvantelRevertirRequest(@NotNull Input params) throws DataException {
        String requestAvantel;


        // A. VALIDAR PARÁMETROS DE ENTRADA
        if (params.getControlNumber() == null)
            throw new DataException("-2", "El OID es un parámetro obligatorio");


        String oid = params.getControlNumber();
        if (oid.length() < OIL_LENGHT)
            oid = zerofill(oid, OIL_LENGHT);

        // B. ARMAR EL MENSAJE
        //   01. CREDIPPE
        //   02. Entidad
        //   03. OID

        StringBuilder sb = new StringBuilder();
        sb.append("REVPPE:");
        sb.append("ENT=").append(this.config.getEntity()).append(SEPARATOR);
        sb.append("OID=").append(oid).append(SEPARATOR);
        sb.trimToSize();
        requestAvantel = sb.toString();

        return requestAvantel;
    }

    // Procesa la respuesta del operador para transacciones de recargas
    private Data processResponseAvantelRequest(String[] respOperPipe) throws DataException {
        // Transformar la respuesta
        Data response = new Data();
        // Validar el tamaño de las respuesta
        if (respOperPipe == null)
            respOperPipe = new String[1];

        if ((respOperPipe[0].split(":")[1]).equals("EST=0")) {
            if (respOperPipe.length != 24)
                throw new DataException("-3", "La respuesta recibida del operador está incompleta. Campos esperados: 24. Campos recibidos: " + respOperPipe.length);
        } else {
            if (respOperPipe.length != 22)
                throw new DataException("-3", "La respuesta recibida del operador está incompleta. Campos esperados: 22. Campos recibidos: " + respOperPipe.length);
        }

        if ((respOperPipe[0].split(":")[1]).equals("EST=0")) {
            String msn = this.config.getErrors().get((respOperPipe[0].split(":")[1]).split("=")[1]);
            response.setCode(msn.split("@#")[0]);
            response.setMessage(msn.split("@#")[1]);
            response.setTransactionUid(respOperPipe[respOperPipe.length - 1].split("=")[1]);
        } else {
            String error = null;
            Boolean error_mapeado = true;
            try {
                error = this.config.getErrors().get((respOperPipe[0].split(":")[1]).split("=")[1]);
            } catch (Exception e) {
                error_mapeado = false;
            }
            if (error_mapeado) {
                if (error != null) {
                    log.info("Mapeando error " + (respOperPipe[0].split(":")[1]).split("=")[1] +
                            " a " + this.config.getErrors().get((respOperPipe[0].split(":")[1]).split("=")[1]));
                    response.setMessage(error.split("@#")[1]);
                    response.setCode(error.split("@#")[0]);
                } else {
                    log.info("Codigo de error" + (respOperPipe[0].split(":")[1]).split("=")[1] + " no configurado");
                    response.setMessage(this.config.getErrors().get("99").split("@#")[1]);
                    response.setCode(this.config.getErrors().get("99").split("@#")[0]);
                }
            } else {
                log.info("Codigo de error" + respOperPipe[0] + " no configurado");
                response.setMessage(this.config.getErrors().get("99").split("@#")[1]);
                response.setCode(this.config.getErrors().get("99").split("@#")[0]);

            }
            response.setTransactionUid(respOperPipe[respOperPipe.length - 1].split("=")[1]);
        }
        //Procesar la respuesta del operador
        //log.info("Response Avantel[State]: " + respOperPipe[13]);
        //String error = this.config.getErrors().get(PREFIX_RECHARGE + respOperPipe[13]);
        //String[] respuesta = error.split(ERRORS_SEPARATOR);

        return response;
    }

    // Procesa la respuesta del operador para transacciones de reversion de recargas
    private Data processResponseReversionAvantelRequest(String[] respOperPipe) throws DataException {
        // Transformar la respuesta
        Data response = new Data();
        // Validar el tamaño de las respuesta
        if (respOperPipe == null)
            respOperPipe = new String[1];

        if (respOperPipe.length != 15)
            throw new DataException("-3", "La respuesta recibida del operador está incompleta. Campos esperados: 15. Campos recibidos: " + respOperPipe.length);

        if ((respOperPipe[0].split(":")[1]).equals("EST=0")) {
            log.info("[Part.ReversionAvantel] Respuesta Exitosa Reverso");
            //retryReversal = false;
            response.setReverse(false);
        } else {
            log.info("[Part.ReversionAvantel] Respuesta Rechazada del Reverso");
            response.setReverse(true);
        }
        //Procesar la respuesta del operador
        //log.info("Response Avantel[State]: " + respOperPipe[13]);
        //String error = this.config.getErrors().get(PREFIX_RECHARGE + respOperPipe[13]);
        //String[] respuesta = error.split(ERRORS_SEPARATOR);

        return response;
    }


    // Realizar recarga " + config.getServiceName() + "
    @Override
    public Data procesarRecarga(Input parametros) throws ServiceException {
        Data respuesta = new Data();
        Socket connectAvantel = null;
        long createComIn = 0;
        long createComFin = 0;


        try {
            // A. GENERAR EL MENSAJE A ENVIAR AL OPERADOR
            log.info("Generar el mensaje de comunicación: Transacción de recarga.");
            String sendMesg = createAvantelRequest(parametros);

            log.info("Mensaje generado: " + sendMesg);

            // B. CONECTAR AL OPERADOR
            log.info("Conectándose al operador con la IP: " + this.config.getIp() + " y puerto: " + this.config.getPort());
            Integer connectionTimeout = this.config.getConexionTimeout();
            Integer requestTimeout = this.config.getPeticionTimeout();
            createComIn = System.currentTimeMillis();
            connectAvantel = this.avantelRepository.openSockeConnection(this.config.getIp(), this.config.getPort(), connectionTimeout, requestTimeout);
            createComFin = System.currentTimeMillis() - createComIn;

            // Verificar si se superó el tiempo máximo de conexión
            if (createComFin > this.config.getConexionTimeout()) {
                // Forzar el Timeout, por tiempo de comunicación soprepasado
                throw new SocketException("Tiempo de conexión superado para enviar la transacción...[Operador " + config.getServiceName() + "]");
            }
            log.info("Conexión exitosa al operador.");
            log.info("Tiempo empleado en crear la conexión para la comunicación con " + config.getServiceName() + " = " + createComFin + " [Tiempo máximo Configurado = " + connectionTimeout + " ].");

            // C. TRANSMITIR EL MENSAJE
            log.info("Enviando transacción a " + config.getServiceName() + "");
            this.avantelRepository.sendRequest(connectAvantel, sendMesg);
            log.info("Request " + config.getServiceName() + " --> " + sendMesg.replace("\n", ""));

            // D. OBTENER LA RESPUESTA
            log.info("Leyendo respuesta de " + config.getServiceName() + "");
            String respOper = this.avantelRepository.getResponse(connectAvantel);
            log.info("Response " + config.getServiceName() + " --> " + respOper);

            // Procesar la respuesta del operador
            String respOperPipe[] = respOper.split("\\?");
            respuesta = processResponseAvantelRequest(respOperPipe);
            respuesta.setReverse(Boolean.FALSE);

        } catch (DataException ex) {
            // ERROR CON LOS DATOS SUMINISTRADOS
            respuesta.setReverse(Boolean.FALSE);
            log.error(ex.getMensaje());
            throw ex;

        } catch (SocketException ex) {
            respuesta.setReverse(Boolean.FALSE);
            // PROBLEMAS CON EL TIMEOUT del servidor se debe reintentar la transacción
            String message = "Error de TIMEOUT con el operador " + config.getServiceName() + ". Causa: [SocketException] " + ex;
            throw new ServiceException(ErrorType.PROCESSING, "99", message);

        } catch (IOException ex) {
            // PROBLEMAS CON EL TIMEOUT del servidor se debe reintentar la transacción
            log.error("Error de TIMEOUT con el operador " + config.getServiceName() + ". Causa: [IOExeption] " + ex);
            respuesta.setReverse(Boolean.TRUE);
            respuesta.setTransactionTime(createComFin);
        } catch (Exception ex) {
            // DIRECCIÓN IP INVÁLIDA
            respuesta.setReverse(Boolean.FALSE);
            String message = "Error inesperado conectandose al operador: " + ex.getMessage();
            log.error(message);

            throw new ServiceException(ErrorType.PROCESSING, "99", message);
        } finally {
            // Cerrar la conexión al Socket
            log.info("Cerrando socket de comunicación");
            this.avantelRepository.closeSocketConnection(connectAvantel);

            // Se calcula el tiempo empleado en el envio-recepcion con el operador
            createComFin = System.currentTimeMillis() - createComIn;
            log.info("Tiempo empleado en el envío/respuesta operador:  " + createComFin);
            respuesta.setTransactionTime(createComFin);
            respuesta.setDate(new Date());
        }

        return respuesta;

    }

    // Revertir recarga " + config.getServiceName() + "
    @Override
    public Data revertirRecarga(Input parametros) throws ServiceException {
        Data respuesta = new Data();
        Socket connectAvantel = null;
        long createComIn = 0;
        long createComFin;

        this.validateInputRequestReverse(parametros);

        /*Se inicializa la variable retryReversal en true para que en caso de que sea necesario reversar realice al menos un intento*/
        int reversionCounter = 1;
        boolean retryReversal = true;
        if (parametros.getReverse()) {
            while (retryReversal) {
                try {
                    log.info("[Part.ReversionAvantel] Prepare reversion Avantel");
                    log.info("[Part.ReversionAvantel] Reversando transaccion......Avantel");
                    log.info("[Part. ReversionAvantel]: Intento de reversion numero ==>" + this.config.getReversionAttempts());

                    // A. GENERAR EL MENSAJE A ENVIAR AL OPERADOR
                    log.info("Generar el mensaje de comunicación: Transacción de reversion de recarga.");
                    String sendMesg = createAvantelRevertirRequest(parametros);

                    // B. CONECTAR AL OPERADOR
                    log.info("Conectándose al operador con la IP: " + this.config.getIp() + " y puerto: " + this.config.getPort());
                    Integer connectionTimeout = this.config.getReversionTimeout();
                    Integer requestTimeout = this.config.getPeticionReintentoTimeout();
                    createComIn = System.currentTimeMillis();
                    connectAvantel = this.avantelRepository.openSockeConnection(this.config.getIp(), this.config.getPort(), connectionTimeout, requestTimeout);
                    createComFin = System.currentTimeMillis() - createComIn;

                    // Verificar si se superó el tiempo máximo de conexión
                    if (createComFin > this.config.getConexionTimeout()) {
                        // Forzar el Timeout, por tiempo de comunicación soprepasado
                        throw new SocketException("Tiempo de conexión superado para enviar la transacción...[Operador " + config.getServiceName() + "]");
                    }

                    log.info("Conexión exitosa al operador.");
                    log.info("Tiempo empleado en crear la conexión para la comunicación con " + config.getServiceName() + " = " + createComFin + " [Tiempo máximo Configurado = " + this.config.getConexionTimeout() + " ].");

                    // C. TRANSMITIR EL MENSAJE
                    log.info("Enviando transacción de reversion de recarga a " + config.getServiceName() + "");
                    this.avantelRepository.sendRequest(connectAvantel, sendMesg);
                    log.info("Request Reversion de recarga en " + config.getServiceName() + " --> " + sendMesg.replace("\n", ""));

                    // D. OBTENER LA RESPUESTA
                    log.info("Leyendo respuesta de la reversion de recarga en " + config.getServiceName() + "");
                    String respOper = this.avantelRepository.getResponse(connectAvantel);
                    log.info("Response de la reversionde recarga de " + config.getServiceName() + " --> " + respOper);

                    // Procesar la respuesta del operador
                    String respOperPipe[] = respOper.split("\\?");
                    respuesta = processResponseReversionAvantelRequest(respOperPipe);
                    retryReversal = respuesta.getReverse();
                    respuesta.setReverse(null);
                    reversionCounter++;

                    if (reversionCounter == this.config.getReversionAttempts()) {
                        retryReversal = false;
                        if (respOperPipe.length <= 0) {
                            log.error("No se recibe respuesta de Reversion");
                        }
                    }
                } catch (DataException ex) {
                    // PROBLEMAS CON LA DATA del servidor se debe reintentar la transacción
                    String message = ex.getMessage();
                    log.error(ex.getMessage());
                    throw new ServiceException(ErrorType.DATA, "99", message);

                } catch (Exception ex) {
                    String message = ex.getMessage();
                    log.error(ex.getMessage());
                    throw new ServiceException(ErrorType.PROCESSING, "99", message);

                } finally {
                    // Cerrar la conexión al Socket
                    log.info("Cerrando socket de comunicación");
                    this.avantelRepository.closeSocketConnection(connectAvantel);

                    // Se calcula el tiempo empleado en el envio-recepcion con el operador
                    createComFin = System.currentTimeMillis() - createComIn;
                    log.info("Tiempo empleado en el envío/respuesta operador:  " + createComFin);
                    respuesta.setTransactionTime(createComFin);
                    respuesta.setDate(new Date());
                    respuesta.setReverse(null);
                }
            }
        } else {
            log.info("[Part.ReversionAvantel] NO SE REVERSA LA TRANSACCION");
            /*Se almacena este key para que cuando se invoque el flujo que valida si se reversa o no, no se registre la transaccion*/
        }

        return respuesta;

    }

    private String zerofill(String field, int complemento) {
        String cadena = "";
        int leftforfill = complemento - field.length();
        if (leftforfill > 0) {
            for (int i = 0; i < leftforfill; i++) {
                cadena += "0";
            }
            cadena = cadena + field;
        }

        return cadena;
    }

    private String getVigencia() {
        Integer vigencia = this.config.getRechargeValidity();
        log.info("Vigencia => " + vigencia + " Dias");

        final DateFormat sdf = new SimpleDateFormat("ddMMyyyy");

        final Calendar fechaActual = Calendar.getInstance();
        fechaActual.add(Calendar.DAY_OF_MONTH, vigencia);

        return sdf.format(fechaActual.getTime());
    }

    // Validar parámetros de entrada al servicio de recarga
    private void validateInputRequest(Input parametros) throws DataException {
        // Número de recarga: Obligatorio, Número positivo y de hasta 12 dígitos
        if ((parametros.getRechargeNumber() == null) ||
                (parametros.getRechargeNumber() <= 0) ||
                (Long.toString(parametros.getRechargeNumber()).length() > MAX_DIGITOS_RECHARGE_NUMBER))
            throw new DataException("-2", "El NÚMERO DE RECARGA provisto es inválido");

        // Número de control: Obligatorio, Número positivo y de hasta 12 dígitos
        if (parametros.getControlNumber() == null)
            throw new DataException("-2", "El NÚMERO DE CONTROL provisto es inválido");

        // Monto: Obligatorio, Número positivo y de hasta 6 dígitos
        if ((parametros.getAmount() == null) ||
                (parametros.getAmount() <= 0) ||
                (Long.toString(parametros.getAmount()).length() > MAX_DIGITOS_AMOUNT))
            throw new DataException("-2", "El MONTO DE LA RECARGA provisto es inválido");
    }

    // Validar parámetros de entrada al servicio de recarga
    private void validateInputRequestReverse(Input parametros) throws DataException {


        // Número de control: Obligatorio, Número positivo y de hasta 12 dígitos
        if (parametros.getControlNumber() == null)
            throw new DataException("-2", "El NÚMERO DE CONTROL provisto es inválido");

        // Reverse: Obligatorio, Valor Boolean
        if ((parametros.getReverse() == null))
            throw new DataException("-2", "LA VARIABLE REVERSE provista es inválido");


    }

}
