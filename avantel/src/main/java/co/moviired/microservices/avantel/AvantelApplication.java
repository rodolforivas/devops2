package co.moviired.microservices.avantel;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/*
 * Copyright @2018. SBD, SAS. Todos los derechos reservados.
 *
 * @author RIVAS, Rodolfo
 * @version 1, 2018-11-02
 * @since 1.0
 */
@SpringBootApplication
@Slf4j
public class AvantelApplication {

    private static final String LOG_LINE = "-------------------------------------------";

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(AvantelApplication.class);
        app.run(args);

        log.info("");
        log.info(LOG_LINE);
        log.info("Avantel: API REST - Launched [OK]");
        log.info(LOG_LINE);
        log.info("");
    }

}