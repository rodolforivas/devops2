package co.moviired.microservices.avantel.exception;


import co.moviired.microservices.avantel.model.enums.ErrorType;

/*
 * Copyright @2018. SBD, SAS. Todos los derechos reservados.
 *
 * @author RIVAS, Rodolfo
 * @version 1, 2018-11-02
 * @since 1.0
 */
public class CommunicationException extends ServiceException {

    public CommunicationException() {
        super(ErrorType.COMMUNICATION, "408", "Servicio provisto por el operador no disponible");
    }

    public CommunicationException(String codigo, String mensaje) {
        super(ErrorType.COMMUNICATION, "408", mensaje);
    }

    public CommunicationException(Exception e) {
        super(ErrorType.COMMUNICATION, "408", e.getMessage());
    }

}
