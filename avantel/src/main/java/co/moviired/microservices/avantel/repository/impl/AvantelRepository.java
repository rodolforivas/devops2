package co.moviired.microservices.avantel.repository.impl;

import co.moviired.microservices.avantel.repository.IAvantelRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;

/*
 * Copyright @2018. SBD, SAS. Todos los derechos reservados.
 *
 * @author RIVAS, Rodolfo
 * @version 1, 2018-11-02
 * @since 1.0
 */
@Repository
@Slf4j
public class AvantelRepository implements IAvantelRepository {

    @Override
    public Socket openSockeConnection(String ip, Integer port, Integer connectionTimeout, Integer requestTimeout) throws IOException {
        Socket connectAvantel = new Socket();
        connectAvantel.setSoTimeout(requestTimeout);
        connectAvantel.connect(new InetSocketAddress(ip, port), connectionTimeout);

        return connectAvantel;
    }

    @Override
    public void closeSocketConnection(Socket connection) {
        try {
            if (connection != null)
                connection.close();
        } catch (Exception exc) {
            log.error("Error cerrando socket de comunicación. Causa: " + exc);
        }
    }

    @Override
    public void sendRequest(Socket connection, String sendMesg) throws IOException {
        log.info("Obteniendo los flujos de escritura desde el operador");
        PrintWriter outOper = new PrintWriter(connection.getOutputStream(), true);
        outOper.println(sendMesg);
    }

    @Override
    public String getResponse(Socket connection) throws IOException {
        log.info("Obteniendo los flujos de lectura desde el operador");

        // Leer en bytes
        BufferedReader inOper = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        StringBuilder respuesta = new StringBuilder();
        String line;
        if ((line = inOper.readLine()) != null) {
            respuesta.append(line);
            respuesta.append("?");
        }
        respuesta.trimToSize();
        return respuesta.toString().replace("??", "?");
    }
}
