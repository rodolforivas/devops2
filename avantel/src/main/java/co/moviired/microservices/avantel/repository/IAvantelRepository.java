package co.moviired.microservices.avantel.repository;

import java.io.IOException;
import java.net.Socket;

/*
 * Copyright @2018. SBD, SAS. Todos los derechos reservados.
 *
 * @author RIVAS, Rodolfo
 * @version 1, 2018-11-02
 * @since 1.0
 */
public interface IAvantelRepository {

    Socket openSockeConnection(String ip, Integer port, Integer connectionTimeout, Integer requestTimeout) throws IOException;

    void closeSocketConnection(Socket connection);

    void sendRequest(Socket connection, String sendMesg) throws IOException;

    String getResponse(Socket connection) throws IOException;

}
