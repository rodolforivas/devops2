package co.moviired.microservices.avantel.model.request;

import co.moviired.microservices.avantel.exception.DataException;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.HashMap;

/*
 * Copyright @2018. SBD, SAS. Todos los derechos reservados.
 *
 * @author RIVAS, Rodolfo
 * @version 1, 2018-11-02
 * @since 1.0
 */
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Input {

    private Long rechargeNumber;
    private String controlNumber;
    private Integer amount;
    private Boolean reverse;

    public static Input parseInput(HashMap<String, Object> parameters) throws DataException {
        Input request = new Input();

        try {
            // Transformar los parámetros al objeto entrada
            Object p = parameters.get("controlNumber");
            if (p != null)
                request.setControlNumber(p.toString());

            p = parameters.get("rechargeNumber");
            if (p != null)
                request.setRechargeNumber(Long.parseLong(p.toString()));

            p = parameters.get("amount");
            if (p != null)
                request.setAmount(Integer.parseInt((String) p));

            p = parameters.get("reverse");
            if (p != null)
                request.setReverse((Boolean) p);


        } catch (Exception e) {
            throw new DataException("-2", e.getMessage());
        }
        return request;
    }
}
