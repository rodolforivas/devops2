package co.moviired.microservices.avantel.model;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.HashMap;

/*
 * Copyright @2018. SBD, SAS. Todos los derechos reservados.
 *
 * @author RIVAS, Rodolfo
 * @version 1, 2018-11-02
 * @since 1.0
 */
@Component
@EnableConfigurationProperties
@ConfigurationProperties
@Data
public class WSConfig {

    @Value("${spring.application.name}")
    private String serviceName;

    // DATOS DE CONEXIÓN
    @Value("${client.socket.ip}")
    private String ip;
    @Value("${client.socket.port}")
    private Integer port;

    // TIMEOUTS
    @Value("${client.socket.timeout.connection}")
    private Integer conexionTimeout;
    @Value("${client.socket.timeout.retriesConnection}")
    private Integer reintentoConexionTimeout;
    @Value("${client.socket.timeout.read}")
    private Integer peticionTimeout;
    @Value("${client.socket.timeout.retriesRead}")
    private Integer peticionReintentoTimeout;
    @Value("${client.socket.timeout.revAvantel}")
    private Integer reversionTimeout;

    // DATOS DE IDENTIFICACIÓN
    @Value("${properties.entity}")
    private String entity;

    // TIPOS DE TRANSACCIÓN
    @Value("${properties.txRecharge}")
    private String txRecharge;

    // VARIABLES
    @Value("${properties.lot}")
    private String lot;
    @Value("${properties.reversionAttempts}")
    private Integer reversionAttempts;
    @Value("${properties.rechargeValidity}")
    private Integer rechargeValidity;

    private HashMap<String, String> errors;
}
