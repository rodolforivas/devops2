package co.moviired.microservices.avantel.model.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.HashMap;

/*
 * Copyright @2018. SBD, SAS. Todos los derechos reservados.
 *
 * @author RIVAS, Rodolfo
 * @version 1, 2018-11-02
 * @since 1.0
 */
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class Request {

    private HashMap<String, Object> meta;
    private HashMap<String, Object> data;
    private RequestSignature requestSignature;

    public Request(HashMap<String, Object> meta, RequestSignature requestSignature, HashMap<String, Object> data) {
        super();
        this.meta = meta;
        this.data = data;
        this.requestSignature = requestSignature;
    }
}
