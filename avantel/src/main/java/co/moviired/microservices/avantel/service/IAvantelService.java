package co.moviired.microservices.avantel.service;

import co.moviired.microservices.avantel.exception.ServiceException;
import co.moviired.microservices.avantel.model.request.Input;
import co.moviired.microservices.avantel.model.response.Data;

/*
 * Copyright @2018. SBD, SAS. Todos los derechos reservados.
 *
 * @author RIVAS, Rodolfo
 * @version 1, 2018-11-02
 * @since 1.0
 */
public interface IAvantelService {

    // Realizar recarga AVANTEL
    Data procesarRecarga(Input parameters) throws ServiceException;

    // Revertir recarga AVANTEL
    Data revertirRecarga(Input parameters) throws ServiceException;


}
