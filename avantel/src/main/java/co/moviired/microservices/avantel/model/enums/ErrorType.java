package co.moviired.microservices.avantel.model.enums;

/*
 * Copyright @2018. SBD, SAS. Todos los derechos reservados.
 *
 * @author RIVAS, Rodolfo
 * @version 1, 2018-11-02
 * @since 1.0
 */
public enum ErrorType {
    CONFIGURATION, COMMUNICATION, DATA, PROCESSING, INTEGRATOR
}
