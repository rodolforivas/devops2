## Integrations-topup - AVANTEL 
Copyrigth @2018. All rights reserved.

Micro servicio de recarga avantel se comunica al operador a traves de un SOCKET 

Si desea modificar el puerto de conexión al operador o su IP te debes ir a la ruta <RUTA_ESTANDAR> en el servidor
```
        /app/config/topup/avantel/application.yml

```
Este documento se encuentra la configuración del microservicio en formato YML, ejemplo
````

## LOGGGER
logging:
  level:
    org.springframework.web: INFO
    co.moviired: INFO


## PATHS TO SERVICES
client:
  socket:
    ip: localhost
    port: 8897
    timeout:
      connection: 40000
      read: 40000
      retries_connection: 40000
      retries_read: 40000
      rev_avantel: 40000
      
## PROPERTIES ESPECÍFICOS
properties:
  entity: "0006"
  txRecharge: 100
  reversionAttempts: 3
  lot: 1394
  rechargeValidity: 10
  
````
Para ejecutar el docker:
```
docker run -d  --name topup-avantel -p <PUERTO_LOCAL>:8080  -v /app/config/topup/avantel/application.yml:/app/config/application.yml registry.gitlab.com/movii-org/integrations-topup/avantel:latest

```
Para visualizar el log
```
docker logs --follow topup-avantel
```

Desarrollado por Soluciones en Bases de Datos, SAS. Copyrigth @2018.