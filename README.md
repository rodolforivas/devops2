## Moviired - devOPs

Integrations-topup es un proyecto que engloba todo los microservicios de recargas de los operadores
```
- Avantel
- Comcel
- Conexred
- Dime
- Directv
- ETB
- Exito
- Mahindra
- Movistar
- Tigo
- Virgin
```

#### Archivo de configuración

En cada ambiente de Movii debe existir una repositorio que contenga todo los archivos de configuración de los
operadores
```
SERVIDOR CALIDAD

    /app/config/topup/
            operador1/application.yml
            operador2/application.yml
            operador3/application.yml
            
SERVIDOR PRODUCCION

    /app/config/topup/
            operador1/application.yml
            operador2/application.yml
            operador3/application.yml

```

En este archivo se encuentra la configuración de los datos modificables del servicio.
un archivo YML que posee el siguiente formato estandar
```
    # SERVER 
        -- Configuración del servidor tomcat embebido  
            * puerto por donde escuchara
            * contexto
            ...
    ## APPLICATION
        -- Configuración del aplicativo a ejecutar
            * Nombre del servicio
            * version del aplicativo
            * Servicios expuesto (Rest, Iso, Soap) 
                - Metodo expuestos 
                - Puerto del Socket
                 
    ## LOGGGER
        -- Configuración de las trazas dejadas por el aplicativo
            * Nivel de las trazas (DEBUG, INFO, ERROR)
            * Patron de la traza.
            
    ## PATHS TO SERVICES
        -- Configuración de los servición externos que el aplicativo que invocara
            * Se debe colocar el estandar que se va a invocar (ISO, REST o SOAP)
                -- Se configura el timeout dentro de cada standar
                -- Si el estandar es ISO : maneja IP y PORT
                -- Si el estandar es SOAP : maneja ENDPOINT
                -- Si el estandar es REST : maneja URI
    
    ## ERRORES
        -- Se configura el mapeo de errores del operador con los devueltos por los microservicios.
            * Es un MAP de <KEY : VALUE> donde el KEY es el error devuelto por el operador y el VALUE es el error transpormado 
            del servicio. El value tiene un formato < CODIGO | MENSAJE > 
    
    ## PROPERTIES ESPECÍFICOS
        -- En esta sección estaran configurados todo los datos con los que se completa el mensaje entregado al operador.
    
```

Al momento de correr el docker hay q sustituir el application.yml que posee el operador por defecto por el documento del operador especifico,
 y eso se realiza el parametro "-v /app/config/topup/{operador}/application.yml:/app/config/application.yml" 
```
docker run -d  --name topup-{operador} -p {PUERTO_LOCAL}:8080  -v /app/config/topup/{operador}/application.yml:/app/config/application.yml registry.gitlab.com/movii-org/integrations-topup/{operador}:latest
```

DENTRO DE CADA OPERADOR SE ENCUENTRA UN README.MD 
#
Desarrollado por Soluciones en Bases de Datos, SAS. Copyrigth @2018.